<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Settings for 45 North Solutions REST Controller
 * 
 * @version v0.2.2
 * @author andy@45n.co
 */


$CI =& get_instance();

/*
|--------------------------------------------------------------------------
| REST Format
|--------------------------------------------------------------------------
|
| What format should the data be returned in by default?
|
|	Default: json
|
*/
$CI->config->set_item('rest_default_format', 'json');

/*
|--------------------------------------------------------------------------
| REST Enable Keys
|--------------------------------------------------------------------------
|
| When set to true REST_Controller will look for a key and match it to the DB.
| If no key is provided, the request will return an error.
|
|	FALSE
|
*/
$CI->config->set_item('rest_enable_keys', TRUE);


/*
|--------------------------------------------------------------------------
| REST API Key Variable
|--------------------------------------------------------------------------
|
| Which variable will provide us the API Key
|
| Default: X-API-KEY
|
*/
$CI->config->set_item('rest_key_name', 'X-API-KEY');


/*
|--------------------------------------------------------------------------
| REST Enable Logging
|--------------------------------------------------------------------------
|
| When set to true REST_Controller will log actions based on key, date,
| time and IP address. This is a general rule that can be overridden in the
| $CI->method array in each controller.
|
|	FALSE
|
|
*/
$CI->config->set_item('rest_enable_logging', TRUE);


/*
|--------------------------------------------------------------------------
| REST Enable Limits
|--------------------------------------------------------------------------
|
| When set to true REST_Controller will count the number of uses of each method
| by an API key each hour. This is a general rule that can be overridden in the
| $CI->method array in each controller.
|
|	FALSE
|
*/
$CI->config->set_item('rest_enable_limits', TRUE);

/*
|--------------------------------------------------------------------------
| REST Ignore HTTP Accept
|--------------------------------------------------------------------------
|
| Set to TRUE to ignore the HTTP Accept and speed up each request a little.
| Only do this if you are using the $CI->rest_format or /format/xml in URLs
|
|	FALSE
|
*/
$CI->config->set_item('rest_ignore_http_accept', FALSE);


/*
 * Allow REST Method to be changed by HTTP_X_HTTP_METHOD_OVERRIDE header
 */
$CI->config->set_item('enable_emulate_request', TRUE);

/* End of file config.php */
/* Location: ./system/application/config/rest.php */